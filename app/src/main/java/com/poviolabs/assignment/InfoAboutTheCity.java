package com.poviolabs.assignment;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class InfoAboutTheCity extends AppCompatActivity {

    final String WEATHER_API_KEY = "f09ad0d5fe6afa2d986010430aa95df0";
    TextView mTextViewCityName, mTextViewTemperature, mTextViewHumidity, mTextViewDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_about_the_city);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //define views
        mTextViewCityName = (TextView) findViewById(R.id.city_name);
        mTextViewTemperature = (TextView) findViewById(R.id.temperature);
        mTextViewHumidity = (TextView) findViewById(R.id.humidity);
        mTextViewDesc = (TextView) findViewById(R.id.desc);

        //get name of the selected city
        String cityName = getIntent().getStringExtra("cityName");

        //check of we are connected on the internet. If not show warning.
        if (!isConnected()) {
            Snackbar.make(getCurrentFocus(), "You are NOT connected", Snackbar.LENGTH_LONG).show();
        }

        //call AsynTask to perform network operation (Read weather data) on separate thread
        //we encode URL (city name part) to not have the problems with the whitespaces in the name of the city.
        try {
            new HttpAsyncTask().execute("http://api.openweathermap.org/data/2.5/weather?q=" + URLEncoder.encode(cityName, "UTF-8") + "&appid=" + WEATHER_API_KEY);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            StringBuilder responseStrBuilder = new StringBuilder();
            InputStream in = null;

            // HTTP Get
            try {

                URL url = new URL(urls[0]);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

                String inputStr;
                while ((inputStr = streamReader.readLine()) != null)
                    responseStrBuilder.append(inputStr);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                return e.getMessage();
            }

            return responseStrBuilder.toString();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject data = new JSONObject(result);
                mTextViewCityName.setText(data.getString("name"));
                mTextViewTemperature.setText("" + data.getJSONObject("main").getDouble("temp"));
                mTextViewHumidity.setText("" + data.getJSONObject("main").getDouble("humidity"));
                mTextViewDesc.setText(data.getJSONArray("weather").getJSONObject(0).getString("description"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
