package com.poviolabs.assignment;

/**
 * Created by Mark0 on 13/02/16.
 */
public class CityEntity {

    private long id;
    private String name;
    private String temperature;

    public CityEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public CityEntity(long cityId, String cityName) {
        id = cityId;
        name = cityName;
    }

    public CityEntity(long cityId, String cityName, String temp) {
        id = cityId;
        name = cityName;
        temperature = temp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }
}
