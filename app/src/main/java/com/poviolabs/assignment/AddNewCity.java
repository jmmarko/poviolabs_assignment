package com.poviolabs.assignment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class AddNewCity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_city);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set on click listener for the add btn
        findViewById(R.id.btn_add_new_city).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) findViewById(R.id.edit_text_new_city_name);
                String cityName = et.getText().toString();
                if(cityName.trim().equals("")) {
                    Snackbar.make(getCurrentFocus(), "Please insert the name of the city", Snackbar.LENGTH_LONG).show();
                } else {
                    //return value and end activity
                    Intent output = new Intent();
                    output.putExtra("cityName", cityName);
                    setResult(Activity.RESULT_OK, output);
                    finish();
                }
            }
        });


    }

}
