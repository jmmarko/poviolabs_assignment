package com.poviolabs.assignment;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.poviolabs.assignment.DatabaseContract.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

public class MainActivity extends AppCompatActivity {

    final String WEATHER_API_KEY = "f09ad0d5fe6afa2d986010430aa95df0";
    private static final int ADD_NEW_CITY_REQUEST = 1;

    List<CityEntity> mDataset;
    RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeView;

    TextView mTextViewEmptyListMsg;
    DatabaseHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //setup the database
        mDbHelper = new DatabaseHelper(getBaseContext());

        //prepare the dataset
        mDataset = new ArrayList<CityEntity>();

        //read data from the database
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //Define a projection that specifies which columns from the database
        //you will actually use after this query.
        String[] projection = {
                City._ID,
                City.COLUMN_NAME_TITLE
        };

        Cursor c = db.query(
                City.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (c.moveToNext()) {
            long cityId = c.getLong(
                    c.getColumnIndexOrThrow(City._ID)
            );
            String cityName = c.getString(
                    c.getColumnIndexOrThrow(City.COLUMN_NAME_TITLE)
            );
            //create new City object
            CityEntity city = new CityEntity(cityId, cityName, "/");
            mDataset.add(city);
        }

        db.close();

        //define views
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_list_of_cities);
        mTextViewEmptyListMsg = (TextView) findViewById(R.id.text_view_empty_list_msg);
        mSwipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mSwipeView.setEnabled(false);

        //Add scroll listener. Only when the first item on Recycle list is visible enable swipe gesture
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int firstVisibleItem, int visibleItemCount) {
                if(firstVisibleItem == 0) {
                    mSwipeView.setEnabled(true);
                } else {
                    mSwipeView.setEnabled(false);
                }
            }
        });

        mSwipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               mSwipeView.setRefreshing(true);
                //load data via REST
                new HttpAsyncTask().execute();
            }
        });

        //define onItemClickListener for RecycleView
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent showCityData = new Intent(MainActivity.this, InfoAboutTheCity.class);
                showCityData.putExtra("cityName", mDataset.get(position).getName());
                startActivity(showCityData);
            }

            @Override
            public void onItemSwing(View view, final int position) {
                //delete the swiped city and show Snackbar
                final String cityname = mDataset.get(position).getName();
                boolean success = removeFromDatabase(mDataset.get(position).getId());
                if(success) {
                    mAdapter.removeItem(position);
                } else {
                    //write a message.
                }

                Snackbar.make(getCurrentFocus(),"Deleted: " + cityname, Snackbar.LENGTH_LONG).setAction("Undo", new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        long id = storeToDatabase(cityname);
                        CityEntity c = new CityEntity(id, cityname, "/");
                        mAdapter.addItem(position, c);
                        isDatasetEmpty();
                    }
                }).show();

                //check if dataset is empty
                isDatasetEmpty();


            }
        }));

        //check if the dataset is empty
        isDatasetEmpty();

        //setup the RecycleView

        //use linear manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //specify an adapter
        mAdapter = new MyAdapter(mDataset);
        mRecyclerView.setAdapter(mAdapter);

        //define tghe add new city btn
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_city);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //start new intent -> call activity AddNewCity, which return the name of the city we want to add
                Intent addNewCityIntent = new Intent(MainActivity.this, AddNewCity.class);
                startActivityForResult(addNewCityIntent, ADD_NEW_CITY_REQUEST);
            }
        });
    }

    public void isDatasetEmpty() {
        if(mDataset.size() == 0) {
            mRecyclerView.setVisibility(View.INVISIBLE);
            mTextViewEmptyListMsg.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mTextViewEmptyListMsg.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ADD_NEW_CITY_REQUEST) {
            if(resultCode == RESULT_OK) {
                //get the city name and add it to the list.
                String newCityName = data.getStringExtra("cityName");
                //Snackbar.make(getCurrentFocus(), newCityName, Snackbar.LENGTH_LONG).show();
                //store to database
                long id = storeToDatabase(newCityName);
                CityEntity c = new CityEntity(id, newCityName, "/");
                mAdapter.addItem(0, c);
                isDatasetEmpty();
            }
        }
    }

    private boolean removeFromDatabase(long id) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Define 'where' part of query.
        String selection = City._ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(id) };
        // Issue SQL statement.
        db.delete(City.TABLE_NAME, selection, selectionArgs);

        db.close();
        return true;
    }

    private long storeToDatabase(String newCityName) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(City.COLUMN_NAME_TITLE, newCityName);

        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                City.TABLE_NAME,
                null,
                values);

        db.close();
        return newRowId;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            InputStream in = null;

            for(CityEntity c : mDataset) {
                StringBuilder responseStrBuilder = new StringBuilder();
                // HTTP Get
                System.out.println("GETTING DATA FOR: " + c.getName());
                try {

                    String urlString = "http://api.openweathermap.org/data/2.5/weather?q=" +
                            URLEncoder.encode(c.getName(), "UTF-8") + "&appid=" + WEATHER_API_KEY;
                    URL url = new URL(urlString);

                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

                    String inputStr;
                    while ((inputStr = streamReader.readLine()) != null)
                        responseStrBuilder.append(inputStr);

                    urlConnection.disconnect();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    return e.getMessage();
                }

                try {
                    JSONObject data = new JSONObject(responseStrBuilder.toString());
                    if(data.has("main")) {
                        c.setTemperature("" + data.getJSONObject("main").getDouble("temp"));
                        System.out.println("TEMP: " + data.getJSONObject("main").getDouble("temp"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return "success";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            for(CityEntity c : mDataset) {
                System.out.println(c.getName() + " - " + c.getTemperature());
            }

            //update the list
            mAdapter.notifyDataSetChanged();
            mSwipeView.setRefreshing(false);

        }
    }
}
