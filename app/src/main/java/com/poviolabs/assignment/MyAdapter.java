package com.poviolabs.assignment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;


/**
 * Created by Mark0 on 12/02/16.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<CityEntity> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public ViewHolder(View v) {
            super(v);
            mView = v;
        }
    }

    public MyAdapter(List<CityEntity> dataset) {
        mDataset = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_view_cities_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView textViewCityName = (TextView) holder.mView.findViewById(R.id.text_view_city_name);
        textViewCityName.setText(mDataset.get(position).getName());

        TextView textViewCityTemperature = (TextView) holder.mView.findViewById(R.id.text_view_city_temperature);
        textViewCityTemperature.setText(""+mDataset.get(position).getTemperature());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addItem(int position, CityEntity city) {
        mDataset.add(position, city);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }
}
