package com.poviolabs.assignment;

import android.provider.BaseColumns;

/**
 * Created by Mark0 on 13/02/16.
 */
public final class DatabaseContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public DatabaseContract() {}

    /* Inner class that defines the table contents */
    public static abstract class City implements BaseColumns {
        public static final String TABLE_NAME = "city";
        public static final String COLUMN_NAME_TITLE = "title";
    }
}
